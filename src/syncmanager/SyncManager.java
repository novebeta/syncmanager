package syncmanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import org.ini4j.Wini;

public class SyncManager {
    private static void doSshTunnel(String strSshUser, String strSshPassword, String strSshHost, int nSshPort,
            String strRemoteHost, int nLocalPort, int nRemotePort) throws JSchException {
        System.out.println("Try build ssh connection....");
        final JSch jsch = new JSch();
        Session session = jsch.getSession(strSshUser, strSshHost, 22);
        session.setPassword(strSshPassword);
        final Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();
        session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
    }
    public static void main(String[] args) {
//        System.out.println(args.length);
//        for (String s : args) {
//            System.out.println(s);
//        }
        if (args.length != 4) {
            System.out.println("Proper Usage is: SyncManager [scan|resync] cabang tgl_mulai tgl_akhir\n"
                    + "EXAMPLE : SyncManager scan JOG01 01-01-2017 01-02-2017");
            System.exit(0);
        }
        if (!"SCAN".equals(args[0].trim().toUpperCase()) && !"RESYNC".equals(args[0].trim().toUpperCase())) {
            System.out.println("Proper Usage is: SyncManager [scan|resync] cabang tgl_mulai tgl_akhir\n"
                    + "EXAMPLE : SyncManager scan JOG01 01-01-2017 01-02-2017");
            System.exit(0);
        }
        try {
            Wini ini = new Wini(new File("config.ini"));
//            Set<String> sectionNames = ini.keySet();
            String store = args[1].trim().toUpperCase();
            String start_ = args[2];
            String end_ = args[3];
            boolean strSshUse = (boolean) ini.get("SSH", "USE", boolean.class);                  // SSH loging username
            String strSshUser = ini.get("SSH", "USER");                  // SSH loging username
            String strSshPassword = ini.get("SSH", "PASSWORD");                    // SSH login password
            String strSshHost = ini.get("SSH", "HOST");           // hostname or ip or SSH server
            int nSshPort = (int) ini.get("SSH", "PORT", int.class);                                    // remote SSH host port number
            int nLocalPort = (int) ini.get("SSH", "LPORT", int.class);                               // local port number use to bind SSH tunnel
            int nRemotePort = (int) ini.get("SSH", "RPORT", int.class);                              // remote port number of your database
            String strDbPusatUser = ini.get("PUSAT", "USER");                    // database loging username
            String strDbPusatPassword = ini.get("PUSAT", "PASSWORD");                    // database login password
            String strDbPusatName = ini.get("PUSAT", "DBNAME");
            String strDbPusatHost = ini.get("PUSAT", "HOST");
            int strDbPusatPort = (int) ini.get("PUSAT", "PORT", int.class);
            String strDbCabangUser = ini.get(store, "USER");                    // database loging username
            String strDbCabangPassword = ini.get(store, "PASSWORD");                    // database login password
            String strDbCabangName = ini.get(store, "DBNAME");
            String strDbCabangHost = ini.get(store, "HOST");
            int strDbCabangPort = (int) ini.get(store, "PORT", int.class);
            String strRemoteHost = strDbCabangHost;
            Map<String, String> q = (Map<String, String>) ini.get("QUERY");
            ArrayList t = new ArrayList();
            for (Map.Entry<String, String> entry : q.entrySet()) {
//                System.out.println(entry.getKey() + "/" + entry.getValue());
                t.add(new String[]{entry.getKey(), ini.get("COUNT", entry.getValue()),
                    ini.get("UPDATE", entry.getValue())});
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-M-dd");
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(start_));
            Calendar d = Calendar.getInstance();
            d.setTime(sdf.parse(end_));
            if (c.compareTo(d) > 0) {
                System.out.println("Tgl mulai harus lebih kecil dari tanggal selesai.");
                System.exit(1);
            }
            System.out.println("Welcome!!! Proses cek dan resync cabang " + store);
            if (strSshUse) {
                strDbCabangPort = nLocalPort;
                strDbCabangHost = "localhost";
                SyncManager.doSshTunnel(strSshUser, strSshPassword, strSshHost, nSshPort,
                        strRemoteHost, nLocalPort, nRemotePort);
            }
            Connection conn_pusat = null, conn_cabang = null;
            Statement stmt_pusat = null, stmt_cabang = null;
            ResultSet rs_pusat = null, rs_cabang = null;
            try {
                Integer cabang = 0, pusat = 0;
                conn_cabang = DriverManager.getConnection(
                        String.format("jdbc:mysql://%s:%d/%s?useCompression=true&user=%s&password=%s",
                                strDbCabangHost, strDbCabangPort, strDbCabangName,
                                strDbCabangUser, strDbCabangPassword
                        ));
                conn_pusat = DriverManager.getConnection(
                        String.format("jdbc:mysql://%s:%d/%s?useCompression=true&user=%s&password=%s",
                                strDbPusatHost, strDbPusatPort, strDbPusatName,
                                strDbPusatUser, strDbPusatPassword
                        ));
                while (c.compareTo(d) <= 0) {
                    System.out.print(sdf.format(c.getTime()) + " ");
                    for (Iterator<String[]> i = t.iterator(); i.hasNext();) {
                        String[] next = i.next();
                        stmt_pusat = null;
                        stmt_cabang = null;
                        rs_pusat = null;
                        rs_cabang = null;
                        stmt_cabang = conn_cabang.createStatement();
                        String query = next[1];
                        String date_ = sdf1.format(c.getTime());
                        String cmd = String.format(query, store, store, date_);
                        rs_cabang = stmt_cabang.executeQuery(cmd);
                        while (rs_cabang.next()) {
                            cabang = rs_cabang.getInt(1);
                        }
                        stmt_pusat = conn_pusat.createStatement();
                        rs_pusat = stmt_pusat.executeQuery(cmd);
                        while (rs_pusat.next()) {
                            pusat = rs_pusat.getInt(1);
                        }
                        Boolean status = Objects.equals(cabang, pusat);
                        System.out.print("[" + next[0] + "] : " + (status ? "OK"
                                : String.format("SELISIH (%,d)", cabang - pusat)) + " ");
                        if (!status && "RESYNC".equals(args[0].trim().toUpperCase())) {
                            stmt_cabang = null;
                            stmt_cabang = conn_cabang.createStatement();
                            Integer count = stmt_cabang.executeUpdate(String.format(next[2], date_));
                            System.out.print(String.format("RESYCNED (%,d row)", count));
                        }
                    }
                    System.out.println();
                    c.add(Calendar.DATE, 1);
                }
            } catch (SQLException ex) {
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs_cabang != null) {
                    try {
                        rs_cabang.close();
                    } catch (SQLException sqlEx) {
                    } // ignore
                    rs_cabang = null;
                }
                if (stmt_cabang != null) {
                    try {
                        stmt_cabang.close();
                    } catch (SQLException sqlEx) {
                    } // ignore
                    stmt_cabang = null;
                }
                if (rs_pusat != null) {
                    try {
                        rs_pusat.close();
                    } catch (SQLException sqlEx) {
                    } // ignore
                    rs_pusat = null;
                }
                if (stmt_pusat != null) {
                    try {
                        stmt_pusat.close();
                    } catch (SQLException sqlEx) {
                    } // ignore
                    stmt_pusat = null;
                }
            }
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        } catch (JSchException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        System.exit(0);
    }
}
